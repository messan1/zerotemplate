import { getCssText } from "@/shared/utils/stitches.config";
import { AppConfig } from '@/utils/AppConfig';
import Document, { Head, Html, Main, NextScript } from 'next/document';

// Need to create a custom _document because i18n support is not compatible with `next export`.
class MyDocument extends Document {
  // eslint-disable-next-line class-methods-use-this
  render() {
    return (
      <Html lang={AppConfig.locale}>

        <Head>
          <link rel="stylesheet" href="https://unpkg.com/tailwindcss@3.2.4/src/css/preflight.css" />
          <style id="stitches" dangerouslySetInnerHTML={{ __html: getCssText() }} />
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}

export default MyDocument;
