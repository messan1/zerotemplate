
import '../styles/global.css';
import type { AppProps } from 'next/app';

import { MantineProvider } from '@mantine/core';
import { RouterTransition } from '@/components/Routing/RouterTransition';

const MyApp = ({ Component, pageProps }: AppProps) => (
  <>
    <MantineProvider>
      <RouterTransition />
      <Component {...pageProps} />

    </MantineProvider>
  </>
);

export default MyApp;
