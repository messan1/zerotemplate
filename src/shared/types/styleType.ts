export type DisplayType =
  | 'none'
  | 'flex'
  | 'inline-flex'
  | 'block'
  | 'grid'
  | 'inline-grid'
  | 'table'
  | 'inline'
  | 'initial'
  | 'inline-table';

export type BorderStyleType = 'none' | 'solid' | 'dashed' | 'dotted';

export type BorderType =
  | 'border-top-width'
  | 'border-right-width'
  | 'border-left-width'
  | 'border-bottom-width';

export type PositionType =
  | 'absolute'
  | 'relative'
  | 'fixed'
  | 'sticky'
  | 'static';

export type FlexDirectionType =
  | 'row'
  | 'row-reverse'
  | 'column'
  | 'column-reverse';

export type FlexWrapType = 'nowrap' | 'wrap' | 'wrap-reverse';

export type JustifyContentType =
  | 'flex-start'
  | 'flex-end'
  | 'center'
  | 'space-between'
  | 'space-around'
  | 'space-evenly';

export type AlignItemsType = 'flex-start' | 'flex-end' | 'center' | 'stretch';

export type AlignSelfType = 'auto' | 'flex-start' | 'flex-end' | 'center';

export type AlignContentType = 'flex-start' | 'flex-end' | 'center';

export type FlexType = 'none' | 'auto' | 'initial' | 'inherit';

export type EffectType = 'shadow' | 'filter' | 'outline';

export type BgCoverType = 'cover' | 'contain' | 'fill' | 'none' | 'scale-down';

export type BgRepeatType = 'repeat' | 'repeat-x' | 'repeat-y' | 'no-repeat';

export type BlendingType = 'normal' | 'multiply' | 'screen' | 'overlay';

export type TransformType = 'move' | 'scale' | 'rotate' | 'skew';

export type SegmentType = {
  label: any;
  value: any;
};

export type Background = {
  backgroundSize: string;
  backgroundSizeWidth?: string;
  backgroundSizeHeight?: string;
  backgroundImage: string;
  backgroundAttachment: string;
  backgroundRepeat: string;
  backgroundPosition: string;
  backgroundOrigin: string;
  backgroundClip: string;
  backgroundColor: string;
};
