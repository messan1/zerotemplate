import { set as lset } from 'lodash';

import { parseVal } from './UnitUtils';

export type Filters = {
  blur?: string;
  brightness?: string;
  contrast?: string;
  grayscale?: string;
  'hue-rotate'?: string;
  opacity?: string;
  saturate?: string;
  sepia?: string;
  invert?: string;
};

export const extractFilters = (filter: string): object | undefined => {
  const obj = {};
  const individual: any[] = filter.split(' ');
  const filterNames = /(url)|[a-z-]+(?=\()/g;
  individual.forEach((value) => {
    lset(obj, [value.match(filterNames)[0]], parseVal(value).val);
  });
  return obj || undefined;
};
export const filterUnits = (filter: string): string => {
  if (filter.includes('blur')) return 'px';
  if (filter.includes('grayscale')) return '%';
  if (filter.includes('brightness')) return '%';
  if (filter.includes('contrast')) return '%';
  if (filter.includes('hue-rotate')) return 'deg';
  if (filter.includes('saturate')) return '%';
  if (filter.includes('opacity')) return '%';
  if (filter.includes('invert')) return '%';
  if (filter.includes('sepia')) return '%';

  return 'px';
};

export const FilterToString = (filters: object): string => {
  let filter = '';
  if (filters) {
    Object.entries(filters).forEach((item) => {
      filter = `${filter} ${item[0]}(${item[1]}${filterUnits(item[0])})`;
    });
  }
  return filter.trim();
};
