import { set as lset } from 'lodash';

import type { Background } from '../types/styleType';
import { parseVal } from './UnitUtils';

export const getBackgroundList = (obj: Background): Background[] => {
  const finalList: Background[] = [];

  Object.entries(obj).forEach((property) => {
    // split ech property name array of space seperated string and store the first
    // value in field

    if (
      !['backgroundColor', 'backgroundImage', 'backgroundSize'].includes(
        property[0]
      )
    ) {
      const fields = property[1].split(',');

      fields.forEach((field, index) => {
        lset(finalList, [index, property[0]], field);
      });
    }
    if (['backgroundSize'].includes(property[0])) {
      const fields = property[1].split(',');

      fields.forEach((field, index) => {
        if (!['auto', 'cover', 'contain'].includes(field)) {
          lset(finalList, [index, 'backgroundSizeWidth'], field.split(' ')[0]);
          lset(finalList, [index, 'backgroundSizeHeight'], field.split(' ')[1]);
        } else {
          lset(finalList, [index, property[0]], field);
        }
      });
    }
    if (['backgroundImage'].includes(property[0])) {
      const fields = property[1].replace(/\s/g, '').split(',');

      fields.forEach((field, index) => {
        lset(
          finalList,
          [index, property[0]],
          field
            .replaceAll('url(', '')
            .replaceAll(')', '')
            .replaceAll("'", '')
            .replaceAll("'", '')
        );
      });
    }
  });

  return finalList;
};

export const bgSizeTOString = (background: Background): string => {
  if (['auto', 'cover', 'contain'].includes(background.backgroundSize)) {
    return background.backgroundSize;
  }
  const size = `${parseVal(background?.backgroundSizeWidth || '0').val}px ${
    parseVal(background?.backgroundSizeHeight || '0').val
  }px`;
  return size;
};

export const backgroundListOfBgsToString = (background: Background): string => {
  let bg = '';
  if (background.backgroundImage === 'none') {
    bg = 'none';
  } else {
    bg = `

url("${background.backgroundImage
      .replaceAll('url(', '')
      .replaceAll(')', '')
      .replaceAll("'", '')
      .replaceAll("'", '')}")
`;
  }

  bg += `
        ${background.backgroundPosition} / ${bgSizeTOString(background)}
        ${background.backgroundRepeat}
        ${background.backgroundOrigin}
        ${background.backgroundClip}
        ${background.backgroundAttachment}

        
        `;

  return bg;
};

export const rotateToObject = (
  rotate: string
):
  | {
      axis: string;
      value: number;
    }
  | undefined => {
  const obj: {
    axis: string;
    value: number;
  } = { axis: '', value: 0 };
  if (rotate.split(' ').length === 1) {
    lset(obj, ['axis'], 'z');
    lset(obj, ['value'], parseVal(rotate).val);

    return obj;
  }

  const individual = rotate.split(' ');
  if (rotate === 'none') return undefined;
  if (individual.length !== 2) return undefined;
  lset(obj, ['axis'], individual[0]);
  lset(obj, ['value'], parseVal(individual[1] || '0').val);

  return obj;
};

export const rotateToString = (rotate: { axis: string; value: number }) =>
  `${rotate.axis} ${rotate.value}deg`;

export const ScaleToObject = (scale: string) => {
  const individual = scale.split(' ');
  if (scale === 'none') return undefined;
  const obj: any = {};
  lset(obj, ['x'], parseFloat(individual[0] || '1'));
  lset(obj, ['y'], parseFloat(individual[1] || '1'));

  return obj;
};

export const ScaleToString = (scale: { x: number; y: number }) => {
  return `${scale.x} ${scale.y}`;
};

export const TranslateToObject = (translate: string) => {
  const individual = translate.split(' ');
  if (translate === 'none') return undefined;
  const obj: any = {};
  lset(obj, ['x'], parseFloat(individual[0] || '1'));
  lset(obj, ['y'], parseFloat(individual[1] || '1'));
  lset(obj, ['z'], parseFloat(individual[2] || '1'));

  return obj;
};

export const translateToString = (translate: {
  x: number;
  y: number;
  z: number;
}) => {
  if (translate.x === 0 && translate.y === 0 && translate.z === 0) {
    return 'none';
  }
  return `${translate.x}px ${translate.y}px ${translate.z}px`;
};
