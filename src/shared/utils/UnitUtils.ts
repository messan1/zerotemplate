export function parseVal(str: string) {
  let data = '';
  data = str;
  if (typeof str !== 'string') {
    data = `${str}`;
  }
  if (data) {
    const res = data.match(/(-?[\d.]+)([a-z%]*)/);
    return {
      val: parseFloat(res ? res[1] || '0' : '0'),
      unit: res ? res[2] : 'px',
    };
  }
  return {
    val: parseFloat('0'),
    unit: 'px',
  };
}
