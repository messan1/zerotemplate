import { createStitches } from '@stitches/react';

import { BackgroundToStiches } from '@/shared/utils/clxs';
import { filterUnits } from '@/shared/utils/filter';

export const {
  styled,
  css,
  globalCss,
  keyframes,
  getCssText,
  theme,
  createTheme,
  config,
} = createStitches({
  theme: {
    colors: {
      gray500: 'hsl(206,10%,76%)',
      blue500: 'hsl(206,100%,50%)',
      purple500: 'hsl(252,78%,60%)',
      green500: 'hsl(148,60%,60%)',
      red500: 'hsl(352,100%,62%)',
    },
    space: {
      1: '5px',
      2: '10px',
      3: '15px',
    },
    fontSizes: {
      1: '12px',
      2: '13px',
      3: '15px',
    },
    fonts: {
      untitled: 'Untitled Sans, apple-system, sans-serif',
      mono: 'Söhne Mono, menlo, monospace',
    },
    fontWeights: {},
    lineHeights: {},
    letterSpacings: {},
    sizes: {},
    borderWidths: {},
    borderStyles: {},
    radii: {},
    shadows: {},
    zIndices: {},
    transitions: {},
  },
  media: {
    bp1: '(min-width: 260px)',
    bp2: '(min-width: 320px)',
    bp3: '(min-width: 640px)',
    bp4: '(min-width: 768px)',
    bp5: '(min-width: 1024px)',
    bp6: '(min-width: 1280px)',
    bp7: '(min-width: 1536px)',
  },
  utils: {
    m: (value: any) => ({
      margin: value,
    }),
    mt: (value: any) => ({
      marginTop: value,
    }),
    mr: (value: any) => ({
      marginRight: value,
    }),
    mb: (value: any) => ({
      marginBottom: value,
    }),
    ml: (value: any) => ({
      marginLeft: value,
    }),
    mx: (value: any) => ({
      marginLeft: value,
      marginRight: value,
    }),
    my: (value: any) => ({
      marginTop: value,
      marginBottom: value,
    }),

    p: (value: any) => ({
      padding: value,
    }),

    // Border

    borderColor: (value: any) => ({
      borderColor: value,
    }),

    borderYColor: (value: any) => ({
      borderTopColor: value,
      borderBottomColor: value,
    }),
    borderXColor: (value: any) => ({
      borderLeftColor: value,
      borderRightColor: value,
    }),
    borderLColor: (value: any) => ({
      borderLeftColor: value,
    }),
    borderRColor: (value: any) => ({
      borderRightColor: value,
    }),

    borderBColor: (value: any) => ({
      borderBottomColor: value,
    }),

    borderTColor: (value: any) => ({
      borderTopColor: value,
    }),

    border: (value: any) => ({
      borderWidth: value,
    }),

    bordery: (value: any) => ({
      borderTopWidth: value,
      borderBottomWidth: value,
    }),
    borderx: (value: any) => ({
      borderLeftWidth: value,
      borderRightWidth: value,
    }),
    borderl: (value: any) => ({
      borderLeftWidth: value,
    }),
    borderr: (value: any) => ({
      borderRightWidth: value,
    }),

    borderb: (value: any) => ({
      borderBottomWidth: value,
    }),

    bordert: (value: any) => ({
      borderTopWidth: value,
    }),

    pt: (value: any) => ({
      paddingTop: value,
    }),
    pr: (value: any) => ({
      paddingRight: value,
    }),
    pb: (value: any) => ({
      paddingBottom: value,
    }),
    pl: (value: any) => ({
      paddingLeft: value,
    }),
    px: (value: any) => ({
      paddingLeft: value,
      paddingRight: value,
    }),
    py: (value: any) => ({
      paddingTop: value,
      paddingBottom: value,
    }),

    size: (value: any) => ({
      width: value,
      height: value,
    }),

    linearGradient: (value: any) => ({
      backgroundImage: `linear-gradient(${value})`,
    }),

    bg: (value: any) => ({
      background: BackgroundToStiches(value),
    }),

    br: (value: any) => ({
      borderRadius: value,
    }),

    // Position
    inset: (value: any) => ({
      left: value,
      right: value,
      top: value,
      bottom: value,
    }),
    insetX: (value: any) => ({
      left: value,
      right: value,
    }),
    insetY: (value: any) => ({
      top: value,
      bottom: value,
    }),
    topLeftCorner: (value: any) => ({
      top: value,
      left: value,
    }),
    topRightCorner: (value: any) => ({
      top: value,
      right: value,
    }),
    topEdge: (value: any) => ({
      top: value,
      'inset-x': value,
    }),
    leftEdge: (value: any) => ({
      top: value,
      insetY: value,
    }),
    rightEdge: (value: any) => ({
      right: value,
      insetY: value,
    }),
    fillParent: (value: any) => ({
      inset: value,
    }),
    BottomLeftCorner: (value: any) => ({
      bottom: value,
      left: value,
    }),
    BottomEdge: (value: any) => ({
      bottom: value,
      insetX: value,
    }),
    BottomRightCorner: (value: any) => ({
      bottom: value,
      right: value,
    }),

    // shadow
    shadow: (value: any) => ({
      boxShadow: `${value
        .map(
          (shadow: any) =>
            `${`
            ${shadow.inset}
            ${shadow.x}
            ${shadow.y}
            ${shadow.blur}
            ${shadow.spread}
            ${shadow.color}
        `}`
        )
        .join(',')}`,
    }),

    // Filter
    filter: (value: any) => ({
      filter: `${Object.keys(value)
        .map((item) => `${item}(${value[item]}${filterUnits(item)})`)
        .join(' ')}`,
    }),

    backdropFilter: (value: any) => ({
      backdropFilter: `${Object.keys(value)
        .map((item) => `${item}(${value[item]}${filterUnits(item)})`)
        .join(' ')}`,
    }),

    // Transform
    move: (value: any) => ({
      translate: `${value.x || 0} ${value.y || 0} ${value.z || 0}`,
    }),

    rotate: (value: any) => ({
      rotate: `${value.axis} ${value.value}deg`,
    }),
    scale: (value: any) => ({
      scale: `${value.x || 1} ${value.y || 1}`,
    }),

    // Timing function
    easing: (value: any) => ({
      transitionTimingFunction: `cubic-bezier(${value.join(',')})`,
    }),

    spaceX: (value: any) => ({
      '& > :not([hidden]) ~ :not([hidden])': {
        marginRight: `calc(${value} * 0)`,
        marginLeft: `calc(${value} * calc(1-0))`,
      },
    }),

    spaceY: (value: any) => ({
      '& > :not([hidden]) ~ :not([hidden])': {
        marginTop: `calc(${value} * calc(1 - 0))`,
        marginBottom: `calc(${value} * 0)`,
      },
    }),

    '-spaceX': (value: any) => ({
      '& > :not([hidden]) ~ :not([hidden])': {
        marginRight: `calc(${value} * 1)`,
        marginLeft: `calc(${value} * calc(0))`,
      },
    }),

    '-spaceY': (value: any) => ({
      '& > :not([hidden]) ~ :not([hidden])': {
        marginTop: `calc(${value} * calc(1 - 1))`,
        marginBottom: `calc(${value} * 1)`,
      },
    }),
  },
});
