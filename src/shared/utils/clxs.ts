import type { Background, GradientType } from '../types/styleType';
import { bgSizeTOString } from './transform';


const resolveLinearGradient = (linears: GradientType): string => {
  // linearGradient: '19deg, #21D4FD 0%, #B721FF 100%',
  try {

    if (linears.colors && linears.colors.length > 0) {

      let colors: string[] = []
      linears.colors.forEach((color) => {
        colors.push(
          `${color.color} ${color.percentage}%`
        )
      })

      return `linear-gradient( ${linears.angle}deg , ${colors.join(',')})`
    }
  } catch (error) {
    return ""

  }

  return ''

}


export const BackgroundToStiches = (bgs: Background[]) => {
  let bgData: string[] = [];
  let lastcolor = ""
  bgs.forEach((item, index) => {
    if (item?.backgroundImage) {
      if (item?.backgroundImage === "none") {
        if (bgs.length === index + 1) {
          bgData.push(`${resolveLinearGradient(item.linearGradient).length === 0 ? item.backgroundColor : resolveLinearGradient(item.linearGradient)}`);
        }
      } else {

        bgData.push(
          `${`
          
          url('${item.backgroundImage}')
          ${item.backgroundPosition} / ${bgSizeTOString(item)}
          ${item.backgroundRepeat}
          ${item.backgroundOrigin}
          ${item.backgroundClip}
          ${item.backgroundAttachment}
        
          
          `}`
        );
        if (resolveLinearGradient(item.linearGradient).length > 0) {
          bgData.push(


            resolveLinearGradient(item.linearGradient)



          )
        }

        if(bgs.length===index+1){
          lastcolor= item.backgroundColor 
        }

      }
    } else {
      if (item?.backgroundColor) {
        if (bgs.length === index + 1) {
          bgData.push(item.backgroundColor);
        }
      }
    }
  });



  return bgData.join(",")+ ` ${lastcolor}`;


};
